package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

// Define variables
var e *echo.Echo

func main() {
	// Define webserver
	e = echo.New()

	// Add middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Define routes
	e.Static("/", "serve")
	e.GET("/", func(c echo.Context) error {
		return c.String(200, "This is an RFC-8615 compliant /.well-known/ directory. Please go to one of those files/subdirectories to view content.")
	})

	// Start webserver
	e.Logger.Fatal(e.Start(":1337"))
}
