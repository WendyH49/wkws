# WKWS

A simple and lightweight Go webserver that can be used in conjunction with Traefik's PathPrefixes to serve an RFC-8615 compliant global /.well-known/ directory.

## Features

- Opens a webserver on port 1337
- Is *extraordinarily* lightweight (When testing, it was using ~800kb of RAM)
- Gets out of your way

## Deployment

You can deploy this project in two possible ways, with Docker CLI or with Docker Compose. Please note that these are configured to be used by Traefik and without any SSL, you will have to configure that yourself along with adapting it to fit your specific deployment.

### Docker CLI

```bash
  $ docker volume create wkws_data
  $ docker run -d -p 1337 --name wkws --restart=on-failure:3 --network=traefik -v wkws_data:/serve -l traefik.enable=true -l traefik.http.routers.wkws.entrypoints=web -l traefik.http.routers.wkws.rule=PathPrefix(`/.well-known`) registry.gitlab.com/saharacorp/wkws:latest
```

### Docker Compose

```yaml
version: '3'

services:
  app:
    image: registry.gitlab.com/saharacorp/wkws:latest
    restart: on-failure
    networks:
      - traefik
    ports:
      - "1337"
    volumes:
      - data:/serve
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.wkws.entrypoints=web"
      - "traefik.http.routers.wkws.rule=PathPrefix(`/.well-known`)"
      
volumes:
  data:
  
networks:
  traefik:
    external: true
```

```bash
  $ docker-compose up -d # Docker Compose v1 (seperate command)
  # OR
  $ docker compose up -d # Docker Compose v2 (Docker CLI plugin)
```

### Post Deployment
Visit http://<server ip>:1337 to see the server.

## Run Locally

1. Clone the repository
```bash
  $ git clone https://gitlab.com/saharacorp/wkws.git
  # OR
  $ glab repo clone saharacorp/wkws

  $ cd wkws/
```

2. Install dependencies
```bash
  $ go get -u -v ./...
```

3. Build application
```bash
  $ go build -o out/production/WKWS/app main.go
```

4. Run application
```bash
  $ cd out/production/WKWS/
  $ mkdir -p serve/
  $ ./app
```

Any files that are inside of the `serve/` directory will be served on the webroot at [http://localhost:1337](http://localhost:1337).

## FAQ

#### Are there any plans to add any more features?

We are trying to keep this project as light weight as possible, so no new features are planned.

## License

This project is licensed under the [BSD 3-Clause "New" or "Revised" License](https://tldrlegal.com/license/bsd-3-clause-license-(revised)#summary). The fulltext of the license can be found [here](LICENSE).

## Authors

- [Quinn Lane (Identithree)](https://gitlab.com/Identithree)

